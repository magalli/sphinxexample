# Instructions

To generate the basic layout in the ```docs``` directory:
```
sphinx-quickstart docs
```

# At CERN

If you want to deploy documentation in a separate branch following [these
instructions](https://gitlab.cern.ch/help/user/project/pages/getting_started/pages_from_scratch.md), one also have to register the domain following [these instructions](https://how-to.docs.cern.ch/gitlabpagessite/create_site/create_webeos_project/).
